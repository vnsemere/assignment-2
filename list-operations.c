#include <stdio.h>
#include <stdlib.h>

struct Node
{
    int number;
    struct Node *next;
};

// Function prototypes

struct Node *createNode(int num);
void printList(struct Node *head);
void append(struct Node **head, int num);
void prepend(struct Node **head, int num);
void deleteByKey(struct Node **head, int key);
void deleteByValue(struct Node **head, int value);
void insertAfterKey(struct Node **head, int key, int value);
void insertAfterValue(struct Node **head, int searchValue, int newValue);

int main()
{
    struct Node *head = NULL; // Initialize an empty linked list
    int choice, data, key, value;

    //Display menu
    while (1)
    {
        printf("\nLinked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Delete by Key\n");
        printf("5. Delete by Value\n");
        printf("6. Insert After Key\n");
        printf("7. Insert After Value\n");
        printf("8. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        // Based on users choice
        switch (choice)
        {
        case 1:
            printList(head);
            break;
        case 2:
            printf("Enter data to append: ");
            scanf("%d", &data);
            append(&head, data);
            break;
        case 3:
            printf("Enter data to prepend: ");
            scanf("%d", &data);
            prepend(&head, data);
            break;
        case 4:
            printf("Enter key to delete: ");
            scanf("%d", &key);
            deleteByKey(&head, key);
            break;
        case 5:
            printf("Enter value to delete: ");
            scanf("%d", &value);
            deleteByValue(&head, value);
            break;
        case 6:
            printf("Enter key after which to insert: ");
            scanf("%d", &key);
            printf("Enter value to insert: ");
            scanf("%d", &value);
            insertAfterKey(&head, key, value);
            break;
        case 7:
            printf("Enter value after which to insert: ");
            scanf("%d", &value);
            printf("Enter value to insert: ");
            scanf("%d", &data);
            insertAfterValue(&head, value, data);
            break;
        case 8:
            printf("Exiting program.\n");
            exit(0);
        default:
            printf("Invalid choice. Please try again.\n");
        }
    }

    return 0;
}

//function to create a new node
struct Node *createNode(int num)
{
    struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));
    if (newNode == NULL)
    {
        printf("Memory allocation failed.\n");
        exit(1);
    }
    newNode->number = num;
    newNode->next = NULL;
    return newNode;
}

// Print elements of the linked list
void printList(struct Node *head)
{
    struct Node *temp = head;
    if (temp == NULL)
    {
        printf("List is empty.\n");
        return;
    }
    printf("List: ");
    while (temp != NULL)
    {
        printf("%d -> ", temp->number);
        temp = temp->next;
    }
    printf("NULL\n");
}

//Append new data to end of the list
void append(struct Node **head, int num)
{
    struct Node *newNode = createNode(num);
    if (*head == NULL)
    {
        *head = newNode; //set new node as head of the list
        return;
    }
    struct Node *temp = *head;
    while (temp->next != NULL) //Traverse to end of the list
    {
        temp = temp->next;
    }
    temp->next = newNode;
}

//prepend new node to beginning of the list
void prepend(struct Node **head, int num)
{
    struct Node *newNode = createNode(num);
    newNode->next = *head;
    *head = newNode;
}

//delete a node with a given key
void deleteByKey(struct Node **head, int key)
{
    struct Node *temp = *head, *prev = NULL;
    if (temp != NULL && temp->number == key)
    {
        *head = temp->next;
        free(temp);
        printf("Key %d deleted successfully.\n", key);
        return;
    }
    while (temp != NULL && temp->number != key)
    {
        prev = temp;
        temp = temp->next;
    }
    if (temp == NULL)
    {
        printf("Key %d not found.\n", key);
        return;
    }
    prev->next = temp->next;
    free(temp);
    printf("Key %d deleted successfully.\n", key);
}

// delete a node by value
void deleteByValue(struct Node **head, int value)
{
    struct Node *temp = *head, *prev = NULL;
    if (temp != NULL && temp->number == value)
    {
        *head = temp->next;
        free(temp);
        printf("Value %d deleted successfully.\n", value);
        return;
    }
    while (temp != NULL && temp->number != value)
    {
        prev = temp;
        temp = temp->next;
    }
    if (temp == NULL)
    {
        printf("Value %d not found.\n", value);
        return;
    }
    prev->next = temp->next;
    free(temp);
    printf("Value %d deleted successfully.\n", value);
}

//Insert neew node after a specific key
void insertAfterKey(struct Node **head, int key, int value)
{
    struct Node *temp = *head;
    while (temp != NULL && temp->number != key)
    {
        temp = temp->next;
    }
    if (temp == NULL)
    {
        printf("Key %d not found.\n", key);
        return;
    }
    struct Node *newNode = createNode(value);
    newNode->next = temp->next;
    temp->next = newNode;
    printf("Value %d inserted successfully after key %d.\n", value, key);
}

//Insert new node after a specific value
void insertAfterValue(struct Node **head, int searchValue, int newValue)
{
    struct Node *temp = *head;
    while (temp != NULL && temp->number != searchValue)
    {
        temp = temp->next;
    }
    if (temp == NULL)
    {
        printf("Value %d not found.\n", searchValue);
        return;
    }
    struct Node *newNode = createNode(newValue);
    newNode->next = temp->next;
    temp->next = newNode;
    printf("Value %d inserted successfully after value %d.\n", newValue, searchValue);
}
